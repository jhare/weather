'use strict';

weather
  .Services
  .service('Forecast', [ '$http', function ($http) {

    function getForecast(latitude, longitude) {
      var url = [ '/forecast', 'latitude', latitude, 'longitude', longitude ].join('/');

      return $http.get(url);
    }

    this.get = getForecast;
  }]);
