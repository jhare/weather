var appDependencies = [
  'ui.router',
  'weatherDirectives',
  'weatherControllers',
  'weatherServices',
  'weatherValues',
  'weatherPartials'
];

var weather = {
  App: angular.module('weatherApp', appDependencies),
  Directives: angular.module('weatherDirectives', []),
  Controllers: angular.module('weatherControllers', []),
  Services: angular.module('weatherServices', []),
  Values: angular.module('weatherValues', []),
  Partials: angular.module('weatherPartials', [])
};

function setErrorsInServiceFromUiRouter(event) {
  'use strict';
  event.preventDefault();
}

function defaultStateProvider($stateProvider, $urlRouterProvider) {
  'use strict';

  $urlRouterProvider.otherwise('/');

  var weather = {
    'url': '/',
    'templateUrl': '/_weather/weather-app.html'
  };

  var notFound = {
    'url': '/404',
    'templateUrl': '/_weather/404.html'
  };

  $stateProvider.state('weather', weather)
                .state('404', notFound);
}

function debug($rootScope) {
  'use strict';
  //$rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
    //console.log('$stateChangeStart to "' + toState.name + '" from state "'+ fromState.name + '"- fired when the transition begins. toState,toParams : \n', toState, toParams);
  //});

  $rootScope.$on('$stateChangeError', console.log.bind(console));

  //$rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
    //console.log('$stateChangeSuccess to ' + toState.name + '- fired once the state transition is complete.');
  //});

  //$rootScope.$on('$stateNotFound', function (event, unfoundState, fromState, fromParams) {
    //console.log('$stateNotFound ' + unfoundState.to + '  - fired when a state cannot be found by its name.');
    //console.log(unfoundState, fromState, fromParams);
  //});

  //$rootScope.$on('$viewContentLoading', function (event, viewConfig) {
    //runs on individual scopes, so putting it in "run" doesn't work.
    //console.log('$viewContentLoading - view begins loading - dom not rendered', viewConfig);
  //});

  //$rootScope.$on('$viewContentLoaded', function (event) {
    //console.log('$viewContentLoaded - fired after dom rendered', event);
  //});
}

function run($rootScope) {
  'use stict';

  $rootScope.buildFolder = '_weather';
  $rootScope.$on('$stateChangeError', setErrorsInServiceFromUiRouter);

  debug($rootScope);
}

weather
  .App
  .config(['$stateProvider', '$urlRouterProvider', defaultStateProvider])
  .run(['$rootScope', run]);

window.weather = weather;
