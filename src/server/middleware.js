'use strict';

var express = require('express');
var path = require('path');
var compression = require('compression');
var bodyParser = require('body-parser');

function Middleware() {

  function register(app) {
    var sourcePath = path.dirname(__dirname);
    var publicDir = path.join(sourcePath + '/client/');
    var compressOptions = {
      'threshold': 512,
      'memLevel': 9,
      'level': 5
    };
    var urlencodedOptions = {
      'extended': true
    };

    app.use(compression(compressOptions));
    app.use(express.static(publicDir));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded(urlencodedOptions));
  }

  return {
    'register': register
  };
}

module.exports = new Middleware();
