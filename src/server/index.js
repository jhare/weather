'use strict';

var express = require('express');
var resources = require('./resources');
var middleware = require('./middleware');

var weather = express();
var port = 8080;

resources.register(weather);
middleware.register(weather);

console.log('Running on port:', port);

weather.listen(port);
