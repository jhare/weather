'use strict';

var _ = require('lodash');
var utilities = require('./lib/utilities');
var resources = utilities.getDirectories(__dirname + '/resources');

function Resources() {

  function register(app) {

    function registerResource(resource) {

      function registerRoute(route) {
        var resc = new Definition();
        app[route.httpMethod](route.url, resc[route.method]);
      }

      var Definition = require('./resources/' + resource + '/resource');
      var routes = require('./resources/' + resource + '/routes');

      _.each(routes, registerRoute);
    }

    _.each(resources, registerResource);
  }

  return {
    'register': register
  };
}

module.exports = new Resources();
