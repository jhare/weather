'use strict';

var fs = require('fs');
var _ = require('lodash');

function AppResource() {

  function get(request, response, next) {

    function sendTemplate(err, data) {

      if(err) {
        throw new Error(err);
      }

      var indexTemplate = {

      };

      var index = _.template(data);
      response.send(index(indexTemplate));
    }

    var indexFile = __dirname + '/index.template.html';

    fs.readFile(indexFile, sendTemplate);
  }

  return {
    'get': get
  };
}

module.exports = AppResource;
