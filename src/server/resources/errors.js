'use strict';

var errors = {
  'noApiKey': {
    'error': 'No api key supplied'
  },
  'noLatLong': {
    'error': 'No latitude or longitude supplied'
  }
};

module.exports = errors;
