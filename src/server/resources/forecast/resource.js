'use strict';

var Forecast = require('./forecast');
var forecast = new Forecast();

function ForecastResource() {

  function get(request, response, next) {

    function sendData(data) {
      response.send(data);
    }

    function sendError(err) {
      response.send(503, err);
      //next(err);
    }
    
    var latitude = request.params.latitude;
    var longitude = request.params.longitude;

    var getForecast = forecast.get(latitude, longitude);

    getForecast.then(sendData, sendError);

  }

  return {
    'get': get
  };
}

module.exports = ForecastResource;
