'use strict';

var q = require('telq');

var Banger = require('bangers-and-hash');
var errors = require('../errors');

var key = require('./../../lib/key');

function Forecast() {

  function get(latitude, longitude) {

    function sendData(data) {
      getDeffered.resolve(data);
    }

    function sendError(err) {
      getDeffered.reject(err);
    }

    function getForecast(key) {

      var getOptions = {
        'url': buildUrl(key, latitude, longitude)
      };

      q.get(getOptions).then(sendData, sendError);
    }

    var getDeffered = q.defer();

    if(!latitude || !longitude) {
      getDeffered.reject(errors.noLatLong);
    }

    key.get('forecast.io').then(getForecast, sendError);

    return getDeffered.promise;
  }

  function buildUrl(key, latitude, longitude) {
    var banger = new Banger();

    var latAndLong = latitude + ',' + longitude;

    banger
    .protocol('https')
    .host(key.url)
    .resources( [ 'forecast' , key.key, latAndLong ] );

    return banger.url();
  }

  return {
    'get': get
  };
}

module.exports = Forecast;
