'use strict';

var mongoose = require('mongoose');
var q = require('telq');
var dbMongoose = require('telq/dbMongoose');

q.use(dbMongoose);

function Key() {

  function getKey(app) {

    function sendData(key) {
      mongoose.connection.close();
      getDeffered.resolve(key);
    }

    function sendError(err) {
      mongoose.connection.close();
      getDeffered.reject(err);
    }

    var getDeffered = q.defer();

    mongoose.connect('mongodb://localhost/credentials', function(err) {

      if(err) {
        sendError(err);
      }

      var Keys = mongoose.model('keys', { 
        'name': String, 
        'key': String, 
        'url': String 
      });

      var options = {
        'source': Keys,
        'query': {
          'name': app
        },
        'operation': 'findOne'
      };

      q.dbMongoose(options).then(sendData, sendError);
    });

    return getDeffered.promise;
  }

  return {
    'get': getKey
  };
}

module.exports = new Key();
