var fs = require('fs');
var path = require('path');
var  _ = require('lodash');

function getDirectories(srcpath) {

  function isDirectory(file) {
    return fs.statSync(path.join(srcpath, file)).isDirectory();
  }

  return _.filter(fs.readdirSync(srcpath), isDirectory);
}

var utilities = {
  getDirectories: getDirectories
};

module.exports = utilities;
