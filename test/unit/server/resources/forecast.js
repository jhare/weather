'use strict';

var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);

var expect = chai.expect;

var sinon = require('sinon');
var q = require('telq');

var Forecast = require('../../../../src/server/resources/forecast/forecast.js');
var forecast = new Forecast();

var key = require('../../../../src/server/lib/key');

describe('Given I want to get a forecast', function() {

  beforeEach(function() {

    var fakeKeyPromise = q.when({
      'name': 'forecast',
      'key': 'goodKey',
      'url': 'api.forecast.io'
    });

    sinon.stub(key, 'get').withArgs('forecast.io').returns(fakeKeyPromise);
  });

  afterEach(function() {
    key.get.restore();
  });

  describe('And I supply a valid latitude and longitude', function() {

    var latitude;
    var longitude;

    beforeEach(function() {
      latitude = '89';
      longitude = '-12';
    });

    describe('When I get the forecast', function() {

      var forecastResult;

      beforeEach(function() {

        var getOptions = {
          'url': 'https://api.forecast.io/forecast/goodKey/89,-12'
        };

        var forecastData = require('./forecastData');

        sinon.stub(q, 'get').withArgs(getOptions).returns(q.when(forecastData));
        forecastResult = forecast.get(latitude, longitude);
      });

      afterEach(function() {
        q.get.restore();
      });

      it('Should return forecast data', function(done) {
        var expectedForecast = require('./forecastData');
        expect(forecastResult).to.eventually.deep.equal(expectedForecast).notify(done);
      });
    });

  });

  describe('And I supply an invalid latitude or longitude', function() {

    var latitude;
    var longitude;

    beforeEach(function() {
      latitude = 'a9';
      longitude = '-a2';
    });

    describe('When I get the forcast', function() {

      var forecastResult;

      beforeEach(function() {

        var getReject = new q.Promise(function(res, rej) {
          rej({
            'code': 400,
            'error': 'The given location (or time) is invalid.'
          });
        });

        var getOptions = {
          'url': 'https://api.forecast.io/forecast/goodKey/a9,-a2'
        };

        sinon.stub(q, 'get').withArgs(getOptions).returns(getReject);
        forecastResult = forecast.get(latitude, longitude);
      });

      afterEach(function() {
        q.get.restore();
      });

      it('Should return an invalid location error', function(done) {
        var expectedError = {
          'code': 400,
          'error': 'The given location (or time) is invalid.'
        };
        expect(forecastResult).to.eventually.be.rejected.and.eql(expectedError).notify(done);
      });
    });
  });

  describe('And I do not supply latitude or longitude', function() {

    var latitude;
    var longitude;

    describe('When I get the forecast', function() {

      var forecastResult;

      beforeEach(function() {
        forecastResult = forecast.get(latitude, longitude);
      });

      it('Should return a no latitude or longitude error', function(done) {
        var expectedError = {
          'error': 'No latitude or longitude supplied'
        };
        expect(forecastResult).to.eventually.be.rejected.and.eql(expectedError).notify(done);
      });
    });
  });

});
