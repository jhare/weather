'use strict';

var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);

var expect = chai.expect;

var sinon = require('sinon');
var mongoose = require('mongoose');

var q = require('telq');
var dbMongoose = require('telq/dbMongoose');

q.use(dbMongoose);

var key = require('../../../../src/server/lib/key');

describe('Given I want to get a key from a database', function() {

  describe('And I supply a name to search for', function() {

    var name;

    beforeEach(function() {
      name = 'tester';
    });

    describe('And the key exists', function() {

      beforeEach(function() {

        function fakeConnect(url, callback) {
          callback();
        }

        function returnFakeModel() {
          return fakeModel;
        }

        var fakeModel = {
          'findOne': function(params, cb) {
            cb(false, 'I am calling back something');
          }
        };

        sinon.stub(mongoose, 'connect', fakeConnect);
        sinon.stub(mongoose.connection, 'close', function() {});
        sinon.stub(mongoose, 'model', returnFakeModel);

        var dbMongooseOptions = {
          'source': fakeModel,
          'query': {
            'name': name
          },
          'operation': 'findOne'
        };

        var fakeKey = {
          'name': 'tester',
          'key': 'testerKey',
          'url': 'developer.tester.com'
        };

        sinon.stub(q, 'dbMongoose').withArgs(dbMongooseOptions).returns(q.when(fakeKey));
      });

      afterEach(function() {
        mongoose.connect.restore();
      });

      describe('When I search for the key', function() {

        var keyData;

        beforeEach(function() {
          keyData = key.get(name);
        });

        it('Should return the record for the key', function(done) {
          var expectedKey = {
            'name': 'tester',
            'key': 'testerKey',
            'url': 'developer.tester.com'
          };

          expect(keyData).to.eventually.eql(expectedKey).and.notify(done);
        });
      });
    });
  });
});
