'use strict';

describe('Given I want to get forcast data', function() {

  var latitude;
  var longitude;
  var forecastService;
  var http;

  function mockHttp() {
    return {
      'get': sinon.spy()
    };
  }

  beforeEach(module('weatherApp', function($provide) {
    $provide.service('$http', mockHttp);
  }));

  beforeEach(inject(function injectState($http, Forecast) {
    http = $http;
    forecastService = Forecast;
  }));

  describe('And I supply a valid latitude and longitude', function() {

    beforeEach(function() {
      latitude = 98;
      longitude = 45;
    });

    describe('When I request the forscast data', function () {

      var forecastData;

      beforeEach(function() {
        forecastData = forecastService.get(latitude, longitude);
      });

      it('Should call for forecast data correctly', function() {
        var url = '/forecast/latitude/' + latitude + '/longitude/' + longitude;

        expect(http.get).to.have.been.calledWith(url);
      });
    });
  });

  describe('And I supply an invalid latitude or longitude', function () {

    beforeEach(function() {
      latitude = 9898;
      longitude = 4545;
    });
    
    describe('When I request the forscast data', function () {

      var forecastData;

      beforeEach(function() {
        forecastData = forecastService.get(latitude, longitude);
      });

      it('Should call for forecast data incorrectly', function() {
        var url = '/forecast/latitude/' + latitude + '/longitude/' + longitude;

        expect(http.get).to.have.been.calledWith(url);
      });
    });
  });
});
