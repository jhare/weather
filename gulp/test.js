'use strict';

var runSequence = require('run-sequence');

var gulp = require('gulp');

function Test(options) {
  require('./test/server')(options);
  require('./test/client')(options);

  function buildAndTest(cb) {
    runSequence('build', 'test-server', 'test-client', cb);
  }

  gulp.task('test', buildAndTest);
}

module.exports = Test;
