'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');

function Features(options) {

  function buildFeatures() {

    var src = [
      'src/client/features/**/*.js'
    ];

    return gulp
            .src(src)
            .pipe(concat('features.js'))
            .pipe(gulp.dest(options.buildFolder));
  }

  gulp.task('build-features', buildFeatures);
}

module.exports = Features;
