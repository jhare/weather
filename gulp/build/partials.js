'use strict';

var gulp = require('gulp');

function Partials(options) {
  function buildPartials() {
    return gulp
            .src('./src/client/common/' + options.appName + '-app.html')
            .pipe(gulp.dest(options.buildFolder));
  };

  gulp.task('build-partials', buildPartials);
}

module.exports = Partials;
