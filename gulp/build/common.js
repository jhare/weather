'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');

function Common(options) {

  function buildCommon() {
    var appPath = 'src/client/common/' + options.appName + '.js';

    var src = [
      appPath,
      'src/client/common/**/*.js'
    ];

    return gulp
            .src(src)
            .pipe(concat('common.js'))
            .pipe(gulp.dest(options.buildFolder));
  }

  gulp.task('build-common', buildCommon);
}

module.exports = Common;
