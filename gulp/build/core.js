'use strict';

var glob = require('glob');
var gulp = require('gulp');
var gutil = require('gulp-util');
var webpack = require('webpack');

var webpackUglifyPlugin = new webpack.optimize.UglifyJsPlugin({
  'sourceMap': true,
  'compress': {
    'warnings': false
  }
});

function Core(options) {

  function buildCore(callback) {

    function webPackDone(err, stats) {
      if(err) {
        throw new gutil.PluginError('webpack', err);
      }

      //console.log('[webpack]', stats.toString({}));
      callback();
    }

    var coreFolder = glob.sync(options.clientFolder + '/core/**/*.js');
    var commonFolder = glob.sync(options.clientFolder + '/common/**/*.js');
    var featuresFolder = glob.sync(options.clientFolder + '/features/**/*.js');

    var coreEntry = coreFolder.concat(commonFolder).concat(featuresFolder);

    var webpackOptions = {
      'entry':  coreEntry,
      'output': {
        'filename': 'core.js',
        'path': options.buildFolder
      },
      'plugins': [ webpackUglifyPlugin ]
    };

    webpack(webpackOptions, webPackDone)
  }

  gulp.task('build-core', buildCore);
}

module.exports = Core;
