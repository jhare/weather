'use strict';

var gulp = require('gulp');

function Build(options) {

  require('./build/common')(options);
  require('./build/core')(options);
  require('./build/features')(options);
  require('./build/partials')(options);

  gulp.task('build', [ 'build-common', 'build-features', 'build-partials', 'build-core' ]);
}

module.exports = Build;
