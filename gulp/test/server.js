'use strict';

var gulp = require('gulp');
var istanbul = require('gulp-istanbul');
var mocha = require('gulp-mocha');

function TestServer(options) {

  function testServer(cb) {
    function runner() {
      var output = options.artifactsFolder + '/coverage/server';
      
      var mochaOptions = {
        'reporter': 'dot'
      };

      gulp
        .src(options.specs.server.tests)
        .pipe(mocha(mochaOptions))
        .pipe(istanbul.writeReports(output))
        .on('end', cb);
    }

    var istanbulOptions = {
      'includeUntested': false
    };

    gulp
      .src(options.specs.server.sources)
      .pipe(istanbul(istanbulOptions))
      .pipe(istanbul.hookRequire())
      .on('finish', runner);
  }

  gulp.task('test-server', testServer);
}

module.exports = TestServer;
