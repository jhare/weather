'use strict';

var gulp = require('gulp');
var Server = require('karma').Server;

function TestClient(options) {

  function testClient(done) {
    return new Server({
      'configFile': '../../../test/config/karma.conf.js',
      'singleRun': true
    }, done).start();
  }

  gulp.task('test-client', testClient);
}

module.exports = TestClient;
